﻿using System;

namespace Homework_Theme_01
{
    /// <summary>
    ///     Записная книжка.
    /// </summary>
    internal class Program
    {
        public string Name { get; set; }

        public int Age { get; set; }

        public int Height { get; set; }

        public int History { get; set; }

        public int Math { get; set; }

        public int RuLanguage { get; set; }

        /// <summary>
        ///     <ul>
        ///         Вывод в консоль среднего балла по предметам:
        ///         <li>История;</li>
        ///         <li>Математика;</li>
        ///         <li>Русский язык.</li>
        ///     </ul>
        /// </summary>
        public void calcAverageScore()
        {
            var averageScore = (double) (History + Math + RuLanguage) / 3;
            var result = $"Средний балл по предметам: {averageScore}";
            
            Console.SetCursorPosition(Console.WindowWidth / 2 - result.Length / 2, Console.WindowHeight / 2 - 1);
            Console.Write(result);
        }

        private static void Main(string[] args)
        {
            var program = new Program();
            Console.Write("Имя студента: ");
            program.Name = Console.ReadLine();
            Console.Write("Возраст: ");
            program.Age = int.Parse(Console.ReadLine() ?? string.Empty);
            Console.Write("Рост: ");
            program.Height = int.Parse(Console.ReadLine() ?? string.Empty);
            Console.Write("Балл по Истории: ");
            program.History = int.Parse(Console.ReadLine() ?? string.Empty);
            Console.Write("Балл по Математике: ");
            program.Math = int.Parse(Console.ReadLine() ?? string.Empty);
            Console.Write("Балл по Русскому языку: ");
            program.RuLanguage = int.Parse(Console.ReadLine() ?? string.Empty);

            program.calcAverageScore();
        }
    }
}